//Library link https://github.com/zerokol/eFLL

#include "Arduino.h"
#include <FuzzyRule.h>
#include <FuzzyComposition.h>
#include <Fuzzy.h>
#include <FuzzyRuleConsequent.h>
#include <FuzzyOutput.h>
#include <FuzzyInput.h>
#include <FuzzyIO.h>
#include <FuzzySet.h>
#include <FuzzyRuleAntecedent.h>


uint8_t outputVoltage=A2;
uint8_t inputRectifiedVoltage=A0;
uint8_t inductorCurrent=A1;

uint8_t pulsePin=8;

int outputRefVoltage=440;
int previousError=0;
int currentError=0;
int changeOfError=0;

//uint8_t G1,G2,G3,G4;

int pulseWidth;

Fuzzy* fuzzy = new Fuzzy();

FuzzySet* errorNB = new FuzzySet(-500,  -30,  -30,  -10);
FuzzySet* errorNS = new FuzzySet(-30,  -10, -10, 5);
FuzzySet* errorZ = new FuzzySet(-10, 5, 5,  10);
FuzzySet* errorPS = new FuzzySet(5,  20,   20,   30);
FuzzySet* errorPB = new FuzzySet( 20,   60,  60,  500);

FuzzySet* c_errorNB = new FuzzySet(-500,  -50,  -50,  -20);
FuzzySet* c_errorNS = new FuzzySet(-50,  -10, -10, 0);
FuzzySet* c_errorZ = new FuzzySet(-10, 0, 0,  10);
FuzzySet* c_errorPS = new FuzzySet(-10,  10,   30,   50);
FuzzySet* c_errorPB = new FuzzySet( 30,   50,  50,  500);

FuzzySet* dutyNB = new FuzzySet(0,  0 ,  0 ,  10);
FuzzySet* dutyNS = new FuzzySet(0,  10, 10, 20);
FuzzySet* dutyZ  = new FuzzySet(10, 20, 20, 40);
FuzzySet* dutyPS = new FuzzySet(20, 150,  150,  170);
FuzzySet* dutyPB = new FuzzySet( 150,  170,  170,  220);


void setup()
{
Serial.begin(115200);

pinMode(pulsePin,OUTPUT);
digitalWrite(pulsePin,LOW);
//pinMode(8,OUTPUT);
//analogWrite(8,200);

TCCR4B&=~7;   /// clearing CS0,CS1,CS2
TCCR4B|=1;

//TCCR0B&=7;
//TCCR0B|=1;

//int myPrescaler = 1;         // this could be a number in [1 , 6]. In this case, 3 corresponds in binary to 011.
//TCCR2B |= myPrescaler;


FuzzyInput* error = new FuzzyInput(1);

freeRam ();

error->addFuzzySet(errorNB);
error->addFuzzySet(errorNS);
error->addFuzzySet(errorZ);
error->addFuzzySet(errorPS);
error->addFuzzySet(errorPB);

freeRam ();

if(!fuzzy->addFuzzyInput(error)){
  Serial.println("MEMORY ERROR1");
}

FuzzyInput* c_error = new FuzzyInput(2);

c_error->addFuzzySet(c_errorNB);
c_error->addFuzzySet(c_errorNS);
c_error->addFuzzySet(c_errorZ);
c_error->addFuzzySet(c_errorPS);
c_error->addFuzzySet(c_errorPB);

freeRam ();

if(!fuzzy->addFuzzyInput(c_error)){
  Serial.println("MEMORY ERROR2");
}

FuzzyOutput* dutyCycle = new FuzzyOutput(1);
dutyCycle->addFuzzySet(dutyNB);
dutyCycle->addFuzzySet(dutyNS);
dutyCycle->addFuzzySet(dutyZ);
dutyCycle->addFuzzySet(dutyPS);
dutyCycle->addFuzzySet(dutyPB);

if(fuzzy->addFuzzyOutput(dutyCycle)){
  Serial.println("MEMORY ERROR3");
}

freeRam ();
///////// Fuzzy rules ////////////////


//////////////// Antecedents////////////////////////////////
FuzzyRuleAntecedent* eNBcNB = new FuzzyRuleAntecedent();
eNBcNB->joinWithAND(errorNB, c_errorNB);
FuzzyRuleAntecedent* eNBcNS = new FuzzyRuleAntecedent();
eNBcNS->joinWithAND(errorNB, c_errorNS);
FuzzyRuleAntecedent* eNBcZ = new FuzzyRuleAntecedent();
eNBcZ->joinWithAND(errorNB, c_errorZ);
FuzzyRuleAntecedent* eNScNB = new FuzzyRuleAntecedent();
eNScNB->joinWithAND(errorNS, c_errorNB);
FuzzyRuleAntecedent* eNScNS = new FuzzyRuleAntecedent();
eNScNS->joinWithAND(errorNS, c_errorNS);
FuzzyRuleAntecedent* eZcNB = new FuzzyRuleAntecedent();
eZcNB->joinWithAND(errorZ, c_errorNB);
///////////////////////////////////////////////////////////////
FuzzyRuleAntecedent* eNBcPS = new FuzzyRuleAntecedent();
eNBcPS->joinWithAND(errorNB, c_errorPS);
FuzzyRuleAntecedent* eNScZ = new FuzzyRuleAntecedent();
eNScZ->joinWithAND(errorNS, c_errorZ);
FuzzyRuleAntecedent* eZcNS = new FuzzyRuleAntecedent();
eZcNS->joinWithAND(errorZ, c_errorNS);
FuzzyRuleAntecedent* ePScNB = new FuzzyRuleAntecedent();
ePScNB->joinWithAND(errorPS, c_errorNB);
////////////////////////////////////////////////////////////////

FuzzyRuleAntecedent* eNBcPB = new FuzzyRuleAntecedent();
eNBcPB->joinWithAND(errorNB, c_errorPB);
FuzzyRuleAntecedent* eNScPS = new FuzzyRuleAntecedent();
eNScPS->joinWithAND(errorNS, c_errorPS);
FuzzyRuleAntecedent* eZcZ = new FuzzyRuleAntecedent();
eZcZ->joinWithAND(errorZ, c_errorZ);
FuzzyRuleAntecedent* ePScNS = new FuzzyRuleAntecedent();
ePScNS->joinWithAND(errorPS, c_errorNS);
FuzzyRuleAntecedent* ePBcNB = new FuzzyRuleAntecedent();
ePBcNB->joinWithAND(errorPB, c_errorNB);

////////////////////////////////////////////////////////////////

FuzzyRuleAntecedent* eNScPB = new FuzzyRuleAntecedent();
eNScPB->joinWithAND(errorNS, c_errorPB);
FuzzyRuleAntecedent* eZcPS = new FuzzyRuleAntecedent();
eZcPS->joinWithAND(errorZ, c_errorPS);
FuzzyRuleAntecedent* ePScZ = new FuzzyRuleAntecedent();
ePScZ->joinWithAND(errorPS, c_errorZ);
FuzzyRuleAntecedent* ePBcNS = new FuzzyRuleAntecedent();
ePBcNS->joinWithAND(errorPB, c_errorNS);

/////////////////////////////////////////////////////////////////
FuzzyRuleAntecedent* eZcPB = new FuzzyRuleAntecedent();
eZcPB->joinWithAND(errorZ, c_errorPB);
FuzzyRuleAntecedent* ePScPS = new FuzzyRuleAntecedent();
ePScPS->joinWithAND(errorPS, c_errorPS);
FuzzyRuleAntecedent* ePScPB = new FuzzyRuleAntecedent();
ePScPB->joinWithAND(errorPS, c_errorPB);
FuzzyRuleAntecedent* ePBcZ = new FuzzyRuleAntecedent();
ePBcZ->joinWithAND(errorPB, c_errorZ);
FuzzyRuleAntecedent* ePBcPS = new FuzzyRuleAntecedent();
ePBcPS->joinWithAND(errorPB, c_errorPS);
FuzzyRuleAntecedent* ePBcPB = new FuzzyRuleAntecedent();
ePBcPB->joinWithAND(errorPB, c_errorPB);

///////////////////////////////////////////////////////////////////

/////////////////////// Consequents ////////////////
FuzzyRuleConsequent* thendutyNB = new FuzzyRuleConsequent();
thendutyNB->addOutput(dutyNB);

FuzzyRuleConsequent* thendutyNS = new FuzzyRuleConsequent();
thendutyNS->addOutput(dutyNS);

FuzzyRuleConsequent* thendutyZ = new FuzzyRuleConsequent();
thendutyZ->addOutput(dutyZ);

FuzzyRuleConsequent* thendutyPS = new FuzzyRuleConsequent();
thendutyPS->addOutput(dutyPS);

FuzzyRuleConsequent* thendutyPB = new FuzzyRuleConsequent();
thendutyPB->addOutput(dutyPB);


//////////////////////////////Rules /////////////////////////////////////////

FuzzyRule* fuzzyRule01 = new FuzzyRule(1, eNBcNB, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule01);
FuzzyRule* fuzzyRule02 = new FuzzyRule(2, eNBcNS, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule02);
FuzzyRule* fuzzyRule03 = new FuzzyRule(3, eNBcZ, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule03);
FuzzyRule* fuzzyRule04 = new FuzzyRule(4, eNScNB, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule04);
FuzzyRule* fuzzyRule05 = new FuzzyRule(5, eNScNS, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule05);
FuzzyRule* fuzzyRule06 = new FuzzyRule(6, eZcNB, thendutyNB);
fuzzy->addFuzzyRule(fuzzyRule06);


FuzzyRule* fuzzyRule07 = new FuzzyRule(7, eNBcPS, thendutyNS);
fuzzy->addFuzzyRule(fuzzyRule07);
FuzzyRule* fuzzyRule08 = new FuzzyRule(8, eNScZ, thendutyNS);
fuzzy->addFuzzyRule(fuzzyRule08);
FuzzyRule* fuzzyRule09 = new FuzzyRule(9, eZcNS, thendutyNS);
fuzzy->addFuzzyRule(fuzzyRule09);
FuzzyRule* fuzzyRule10 = new FuzzyRule(10, ePScNB, thendutyNS);
fuzzy->addFuzzyRule(fuzzyRule10);



FuzzyRule* fuzzyRule11 = new FuzzyRule(11, eNBcPB, thendutyZ);
fuzzy->addFuzzyRule(fuzzyRule11);
FuzzyRule* fuzzyRule12 = new FuzzyRule(12, eNScPS, thendutyZ);
fuzzy->addFuzzyRule(fuzzyRule12);
FuzzyRule* fuzzyRule13 = new FuzzyRule(13, eZcZ, thendutyZ);
fuzzy->addFuzzyRule(fuzzyRule13);
FuzzyRule* fuzzyRule14 = new FuzzyRule(14, ePScNS, thendutyZ);
fuzzy->addFuzzyRule(fuzzyRule14);
FuzzyRule* fuzzyRule15 = new FuzzyRule(15, ePBcNB, thendutyZ);
fuzzy->addFuzzyRule(fuzzyRule15);


FuzzyRule* fuzzyRule16 = new FuzzyRule(16, eNScPB, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule16);
FuzzyRule* fuzzyRule17 = new FuzzyRule(17, eZcPS, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule17);
FuzzyRule* fuzzyRule18 = new FuzzyRule(18, ePScZ, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule18);
FuzzyRule* fuzzyRule19 = new FuzzyRule(19, ePBcNS, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule19);

FuzzyRule* fuzzyRule20 = new FuzzyRule(20, eZcPB, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule20);
FuzzyRule* fuzzyRule21 = new FuzzyRule(21, ePScPS, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule21);
FuzzyRule* fuzzyRule22 = new FuzzyRule(22, ePScPB, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule22);
FuzzyRule* fuzzyRule23 = new FuzzyRule(23, ePBcZ, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule23);
FuzzyRule* fuzzyRule24 = new FuzzyRule(24, ePBcPS, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule24);
FuzzyRule* fuzzyRule25 = new FuzzyRule(25, ePBcPB, thendutyPS);
fuzzy->addFuzzyRule(fuzzyRule25);

freeRam ();

delay(1000);

}



void loop()
{
  
  // take output voltage read
   int outputV;

  // for(int i=0;i<10;i++){
    outputV = analogRead(outputVoltage);
    //outputV=outputV/2;
    //delay(1);
    //}
   
 //  outputV= 440;
   
  // calculate error, c_error
   currentError=outputRefVoltage-outputV;
   changeOfError=currentError-previousError;

  // determine duty ration using fuzzy
    fuzzy->setInput(1, currentError);
    fuzzy->setInput(2, previousError);
    fuzzy->fuzzify();
    
    float dutyRatio = fuzzy->defuzzify(1);

    previousError=currentError;

  // multiply duty ratio with rectified voltage
    int rectV= analogRead(inputRectifiedVoltage);

    int temp=  rectV*dutyRatio;

  // compare with inductor current
    int indC = analogRead(inductorCurrent);

    dutyRatio=30;

    
    analogWrite(pulsePin,(int)dutyRatio);


    //calculatePF();
  
    Serial.print("OutputVoltage:");
    Serial.print(outputV);
  //  Serial.print("\tRectifiedVoltage:");
  //  Serial.print(rectV);
  //  Serial.print("\tInductorCurrent:");
  //  Serial.print(indC);
    Serial.print(" Error:");
    Serial.print(currentError);
    Serial.print(" Changeoferror:");
    Serial.print(changeOfError);
    Serial.print(" Duty Ratio:");
    Serial.println(dutyRatio);
  /*
    Serial.print("errorNB = ");
    Serial.print(errorNB->getPertinence());
    Serial.print(",errorNS = ");
    Serial.print(errorNS->getPertinence());
    Serial.print(",errorZ = ");
    Serial.print(errorZ->getPertinence());
    Serial.print(",errorPS = ");
    Serial.print(errorPS->getPertinence());
    Serial.print(",errorPB = ");
    Serial.print(errorPB->getPertinence());
    Serial.print(",c_errorNB = ");
    Serial.print(c_errorNB->getPertinence());
    Serial.print(",c_errorNS = ");
    Serial.print(c_errorNS->getPertinence());
    Serial.print(",c_errorZ = ");
    Serial.print(c_errorZ->getPertinence());
    Serial.print(",c_errorPS = ");
    Serial.print(c_errorPS->getPertinence());
    Serial.print(",c_errorPB = ");
    Serial.println(c_errorPB->getPertinence());
*/
    delay(100);

}

int freeRam ()
{
  extern int __heap_start, *__brkval;
  int v;

  Serial.print("Free RAM:");
  Serial.println((int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval));
}


